package com.test.goeuro;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;

import com.test.serverCommunication.CityResult;
import com.test.serverCommunication.HttpRequest;
import com.test.serverCommunication.JsonHelper;

public class MainActivity extends Activity implements OnClickListener {
	private static final String TAG = "[GoEuro]MainActivity";
	private CalendarView cv;
	private Button searchBtn;
	private Button dateBtn;
	private AutoCompleteTextView source;
	private AutoCompleteTextView destination;

	private ArrayAdapter<String> adapter;
	List locations = new ArrayList();
	private String searchString;
	DownloadDataTask serverThread;
	Location currentLocation; /*
							 * current location to short out autocomplete result
							 * based on distance from current place
							 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_main);
		
		if(!HttpRequest.isNetworkAvailable(this)){
			Toast.makeText(this,"Connection Error, Please turn your internet(Wifi/Mobile_networ) to use the app..", Toast.LENGTH_LONG).show();
		    finish();
		}

		/* Initialising UI components */
		searchBtn = (Button) findViewById(R.id.btn_search);
		dateBtn = (Button) findViewById(R.id.btn_calendar);
		source = (AutoCompleteTextView) findViewById(R.id.actSource);
		destination = (AutoCompleteTextView) findViewById(R.id.actDestination);
		searchBtn.setOnClickListener(this);
		dateBtn.setOnClickListener(this);

		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, locations);
		source.setAdapter(adapter);
		destination.setAdapter(adapter);

		/*
		 * setting listener for TextStateChange, When user enters text in
		 * EditText, listener gets notified
		 */
		source.addTextChangedListener(new CustomTextChangeListener());
		destination.addTextChangedListener(new CustomTextChangeListener());

		setDate(System.currentTimeMillis());
		setCurrentLoccation();
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == searchBtn.getId()) {
			Toast.makeText(MainActivity.this,
					getString(R.string.messsage_search_incomplete),
					Toast.LENGTH_SHORT).show();

		} else if (v.getId() == dateBtn.getId()) {

			startActivityForResult(new Intent(MainActivity.this,
					CalendarDialog.class), 0);
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK && data != null) {
			long dateVal = data.getLongExtra(Constants.DATE_EXTRA, 0);
			setDate(dateVal);
		}
	}

	/* Method to set date in predefined format */
	private void setDate(long dateVal) {
		if (dateVal < System.currentTimeMillis())
			return;

		String dateString = new SimpleDateFormat("dd/MM/yyyy").format(new Date(
				dateVal));
		dateBtn.setText(dateString);
		Log.d(TAG, "dateString = " + dateString);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	class CustomTextChangeListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			Log.d(TAG, "TextWatcher " + s);

			if (source.isPerformingCompletion()) {
				return;
			}
			if (s.length() < 3) {
				return;
			}

			searchString = s.toString();
			if (serverThread != null)
				serverThread.cancel(true);

			serverThread = null;
			serverThread = new DownloadDataTask();
			serverThread.execute(searchString);

		}

	}

	/* parse the response from Server and change the data set accordingly. */

	public void changeDataSet(String JSOnString) {
		Log.d(TAG, "changeDataSet");
		try {
			JSONObject responseObj = new JSONObject(JSOnString);
			JSONArray responseArray = responseObj
					.getJSONArray(JsonHelper.responseKey);
			ArrayList<CityResult> tempList = new ArrayList<CityResult>();
			adapter.clear();

			boolean isLocationAvailable = setCurrentLoccation();
			for (int i = 0; i < responseArray.length(); i++) {
				JSONObject obj = (JSONObject) responseArray.get(i);

				String name = obj.getString(JsonHelper.name);
				int id = obj.getInt(JsonHelper.id);
				JSONObject jobj = obj.getJSONObject(JsonHelper.location);

				long lat = jobj.getLong(JsonHelper.latitude);
				long lng = jobj.getLong(JsonHelper.longitude);

				/*if currrent location available it will calculate distance of current location to location in result*/
				if (isLocationAvailable) {
					Location tempLoc = new Location(
							LocationManager.NETWORK_PROVIDER);
					tempLoc.setLatitude(lat);
					tempLoc.setLongitude(lng);
					tempList.add(new CityResult(id, name, tempLoc,
							(int) currentLocation.distanceTo(tempLoc)));
					tempLoc = null;
				} else {

					tempList.add(new CityResult(id, name, null, 0));
				}
				Log.d(TAG, "name =" + name + "  lat =" + lat + "  lng = " + lng);

			}
			if (isLocationAvailable)
				Collections.sort(tempList);
			for (int i = 0; i < tempList.size(); i++) {
				adapter.add(tempList.get(i).getName());
			}
			adapter.getFilter().filter(searchString, source);

			Log.d(TAG, "Count =" + adapter.getCount());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * Async task to download AutoComplete search suggestions
	 */
	private class DownloadDataTask extends AsyncTask<String, Void, String> {
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... urls) {
			String ret = null;
			try {

				ret = new HttpRequest().getTexts(urls[0]);
			} catch (Exception e) {
				Log.d(TAG, "error==" + e);
				e.printStackTrace();
			}

			return ret;
		}

		@Override
		protected void onPostExecute(String result) {
			Log.d(TAG, "onPostExecute");
			if (result == null)
				return;

			changeDataSet(result);

		}

	}

	/**
	 * Method to set current Location variable, It will first search GPS
	 * location if not available then it will search Network location.
	 */

	public boolean setCurrentLoccation() {
		LocationManager lManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		currentLocation = lManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if (currentLocation == null)
			currentLocation = lManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		if (currentLocation == null)
			return false;

		Log.d(TAG, "getAccuracy = " + currentLocation.getAccuracy()
				+ " getProvider = " + currentLocation.getProvider());
		return true;
	}

}
