package com.test.goeuro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * class to define DateViewer, It will work as a dialog and will return date to calling Activity.
 * @author aankit
 *
 */
public class CalendarDialog extends Activity implements OnClickListener{
	private static final String TAG ="[GoEuro]CalendarDialog";
	private CalendarView cv;
	private Button cancel ;
	private Button save;
	LinearLayout ll;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calendar_layout);
		setTitle("Choose date:");
		cancel =(Button)findViewById(R.id.btncancel);
		save =(Button)findViewById(R.id.btnsave);
		
		cv = (CalendarView) findViewById(R.id.calendarID);
		
		cv.setDate(System.currentTimeMillis());
		
		cv.setOnDateChangeListener(new OnDateChangeListener() {

		        @Override
		        public void onSelectedDayChange(CalendarView view, int year, int month,
		                int dayOfMonth) {
		        	view.getDate();
		        	Log.d(TAG, "dayOfMonth = "+dayOfMonth + "month = "+month +"year = "+year +" view.getDate() = "+view.getDate());
		        	
		        }
		    });
		
		save.setOnClickListener(this);
		cancel.setOnClickListener(this);
		
	}
	
	@Override
	public void onClick(View v) {
		
		if(v.getId() == save.getId()) {
			if(cv.getDate() < System.currentTimeMillis())
			{
				Toast.makeText(CalendarDialog.this,"Please select only future dates..",Toast.LENGTH_SHORT).show();
				return;
			}
			Intent data = new Intent();
            data.putExtra(Constants.DATE_EXTRA, cv.getDate())	;		
			setResult(RESULT_OK, data);
			
		}else if(v.getId() == cancel.getId())	{
			setResult(RESULT_CANCELED, null);
		}
		finish();
	}

	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
}
