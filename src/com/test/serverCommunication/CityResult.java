package com.test.serverCommunication;

import android.location.Location;

/**
 * class which will hold AutoComplete suggesions value from server, like location name,id geoLocation and distance from current lcoation
 * @author aankit
 *
 */
public  class CityResult  implements Comparable<CityResult>{

	private String name;
	private Location location ;
	private int distance;
	private int id;
	
	public int getID() {
		return id;
	}


	public String getName() {
		return name;
	}


	public Location getLocation() {
		return location;
	}

	public int getDistance() {
		return distance;
	}

	
	public CityResult(int mID,String mName, Location mLoc, int mDis) {
		id = mID;
		name = mName;
		location = mLoc;
		distance = mDis;
	}


	@Override
	public int compareTo(CityResult another) {
		if (this.getDistance() == another.getDistance())
			return 0;
		else if(this.getDistance() < another.getDistance())
			return -1;
		return 1;
	}
	
	


}
