package com.test.serverCommunication;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.test.goeuro.Constants;

/**
 * This class fetch data through GET from Server
 * 
 * */
public class HttpRequest {

	DefaultHttpClient httpClient;
	HttpResponse response = null;
	HttpPost httpPost = null;
	HttpGet httpGet = null;
	HttpDelete httpDelete = null;

	public HttpRequest() {
		HttpParams myParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(myParams, 10000);
		HttpConnectionParams.setSoTimeout(myParams, 10000);
		httpClient = new DefaultHttpClient(myParams);
	}

	public void clearCookies() {
		httpClient.getCookieStore().clear();
	}

	public String sendGet(String url) {
		String ret = null;
		httpGet = new HttpGet(url);
		try {
			response = httpClient.execute(httpGet);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			ret = EntityUtils.toString(response.getEntity());
		} catch (IOException e) {
			Log.e("Disk Cache", "" + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		Log.d("HTTPREQUEST:sendGet", "request = " + httpGet.getURI()
				+ "\n response" + ret);

		return ret;
	}

	
	public String getTexts(String arg){
		 String url = Constants.serverURL+arg;
		 return sendGet(url);
		
	}
	
	public static boolean isNetworkAvailable(Context ctx) {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	
}